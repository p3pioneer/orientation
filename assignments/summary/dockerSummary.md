# What Excatly is a Docker??  
Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package. By doing so, thanks to the container, the developer can rest assured that the application will run on any other Linux machine regardless of any customized settings that machine might have that could differ from the machine used for writing and testing the code.  

In a way, Docker is a bit like a virtual machine. But unlike a virtual machine, rather than creating a whole virtual operating system, Docker allows applications to use the same Linux kernel as the system that they're running on and only requires applications be shipped with things not already running on the host computer. This gives a significant performance boost and reduces the size of the application.And To be noted Docker is a **Open Source**.  

![virtual-machine-vs-docker-example-what-is-docker-container-edureka-1](/uploads/5843f249fdbac75eff1ebc899343eb49/virtual-machine-vs-docker-example-what-is-docker-container-edureka-1.png)  
 

# How Does Docker Works??
Docker works on a client-server architecture. It includes the docker client, docker host, and docker registry. The docker client is used for triggering docker commands, docker host is used to running the docker daemon, and docker registry to store docker images.The docker client communicates to docker daemon using a REST API, which internally supports to build, run, and distribute docker containers. Both the client and daemon can run on the same system or can be connected remotely.  
![docker-working-010620](/uploads/414b77a38bfbfe422d8c1b83da07192a/docker-working-010620.png)    
* We use the client (CLI) to issue a build command to the Docker daemon for building a docker image. Based on our inputs the Docker daemon will build an image and save it in Registry, which can either be a local repository or Docker hub.  
* If you don’t want to create an image, just pull it from docker hub built by another user.  
* Finally, If we need to create a running instance of a docker image, issue a run command from CLI to create a docker container. 

# What is Docker Engine ??  
The docker engine is the core of Docker. It uses client-server technology to create and run the containers. It offers two different versions, such as Docker Engine Enterprise and Docker Engine Community.  
![docker-engine-010620](/uploads/3073c826b8428deb0546e54185286f41/docker-engine-010620.png)  



# Docker commands:  
* **To list running containers**:  
      `docker ps`  
    in addition if we want to display running as well as stop containers we use:  
      `docker ps -a`  
*  **Create a container (Without starting it)**:  
    `docker create [image]`  
*  **Delete a container (if it is not running)**:  
    `docker rm [container]`  
*  **To start a container**:  
    `docker start [container]`  
* **To stop a container**:  
    `docker stop [container]`  
* **To stop a running container and start it up again**:  
    `docker restart [container]`  

# Some more Commands:  
![from-vms-to-containers-introducing-docker-containers-for-linux-and-windows-server-13-638](/uploads/cfc630da1f1f92429c110348d995ca44/from-vms-to-containers-introducing-docker-containers-for-linux-and-windows-server-13-638.jpg)  












## Conclusion:  
In short, here’s what Docker can do for you: it makes it easier to develop containerized applications, get more applications run on the same hardware than other technologies, and functions simpler to manage and deploy applications.








