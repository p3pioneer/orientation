# What actually is _Git_?  
Git is an Open Source Distributed Version Control System. Now that’s a lot of words to define Git.Let us break down word by word as:  
* Control system  
This basically means that Git is a content tracker. So Git can be used to store content — it is mostly used to store code due to the other features it provides.  
* Version Control System  
The code which is stored in Git keeps changing as more code is added. Also, many developers can add code in parallel. So Version Control System helps in handling this by maintaining a history of what changes have happened. Also, Git provides features like branches and merges.  
* Distributed Version Control System  
Git has a remote repository which is stored in a server and a local repository which is stored in the computer of each developer. This means that the code is not just stored in a central server, but the full copy of the code is present in all the developers’ computers. Git is a Distributed Version Control System since the code is present in every developer’s computer.  

# Understanding Git Workflow Graph  
![gitoverview](/uploads/4f015c283096b81cf413ee7c77037809/gitoverview.png)  

![what-is-git-what-is-github-git-tutorial-github-tutorial-devops-tutorial-edureka-19-638](/uploads/77955ee5ad07c0ea30f66a298a74e0cc/what-is-git-what-is-github-git-tutorial-github-tutorial-devops-tutorial-edureka-19-638.jpg)  

# What is Gitlab??  
GitLab is a system for managing Git repositories. It's written in Ruby, and allows you to deploy meaningful version control for your code easily.  

# Basic Terms Worth Understanding in Gitlab:  
1. **Repository**:In many ways, you can think of a Git repository as a directory that stores all the files, folders, and content needed for your project. What it actually is, is the object database of the project, storing everything from the files themselves, to the versions of those files, commits, deletions, et cetera. Repositories are not limited by user, and can be shared and copied.  

2. **Fork**:Creates a copy of a repository.  
3. **Branch**:A version of the repository that diverges from the main working project. Branches can be a new version of a repository, experimental changes, or personal forks of a repository for users to alter and test changes.  
4. **Clone**:A clone is a copy of a repository or the action of copying a repository. When cloning a repository into another branch, the new branch becomes a remote-tracking branch that can talk upstream to its origin branch (via pushes, pulls, and fetches).  
5. **Fetch**:By performing a Git fetch, you are downloading and copying that branch’s files to your workstation. Multiple branches can be fetched at once, and you can rename the branches when running the command to suit your needs.  
6. **Pull Requests**:If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request. Pull requests ask the repo maintainers to review the commits made, and then, if acceptable, merge the changes upstream. A pull happens when adding the changes to the master branch.  
7. **Push**:Updates a remote branch with the commits made to the current branch. You are literally “pushing” your changes onto the remote.  


## Basic Git  Handy Commands:  

1. **git config**: This command sets the author name and email address respectively to be used with your commits.  
    ```bash  
    $ git config –global user.name “[name]”
    $ git config –global user.email “[email address]”  
    ```  
2. **git init**: This command is used to start a new repository.  
    ```bash  
    $ git init [repository name]  
    ```
3. **git clone**: This command is used to obtain a repository from an existing URL.  
    ```bash  
    $ git clone [url]  
    ```
4. **git add**: This command adds a file to the staging area.To add more file we can use *.  
    ```bash
    $ git add [file]
    $ git add *  
    ```
5. **git commit**:  This command records or snapshots the file permanently in the version history. 
    ```bash
    $ git commit -m “[ Type in the commit message]”
    ```
6. **git reset**: The first command unstages the file, but it preserves the file contents.The Second command undoes all the commits after the specified commit and preserves the changes locally.The Third command discards all history and goes back to the specified commit.  
    ```bash
    $ git reset [file]
    $ git reset [commit]
    $ git reset –hard [commit] 
    ```
7. **git rm**: This command deletes the file from your working directory and stages the deletion.                
    ```bash
    $ git rm [file]
    ```
8. **git tag**: This command is used to give tags to the specified commit.
    ```bash
    $ git tag [commitID]
    ```
9. **git branch**: The first command lists all the local branches in the current repository.The second command creates a new branch.The third command deletes the feature branch.
    ```bash
    $ git branch
    $ git branch [branch name]
    $ git branch -d [branch name]
    ```
10. **git merge**: This command merges the specified branch’s history into the current branch.
    ```bash
    $ git merge [branch name]
    ```
11. **git remote**: This command is used to connect your local repository to the remote server.
    ```bash
    $ git remote add [variable name] [Remote Server Link]
    ```
12. **git push**: The first command sends the committed changes of master branch to your remote repository.The second command pushes all branches to your remote repository.
    ```bash
    $ git push [variable name] master
    $ git push –all [variable name]
    ```
13. **git pull**: This command fetches and merges changes on the remote server to your working directory.
    ```bash
    $ git pull [Repository Link]
    ```  

![7caaa0455bb5886ed29fedc0b578991f](/uploads/edb2d0809280121478d68402aa04c6d0/7caaa0455bb5886ed29fedc0b578991f.png)    


 


